//
//  ViewController.swift
//  createQRCode
//
//  Created by CCWu on 2019/12/26.
//  Copyright © 2019 吳稚賢. All rights reserved.
//

import UIKit
import Alamofire


class ViewController: UIViewController, UITextFieldDelegate {
  
  var companys:[Company] = []
  let mySendOutJson = ["companyId": "9389434", "companyName": "新明小孩好可怕股份有限公司"]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    downloadData()

  }
  
  @IBOutlet weak var myTextView: UITextView!
  @IBOutlet weak var data: UITextField!
  @IBAction func createButton(_ sender: Any) {
    let myQRCodeContent:String = data.text!
    
    myQRCode.image = generateQRCode(from: myQRCodeContent)
  }
  
  
  @IBOutlet weak var myQRCode: UIImageView!
  
  // images from web url
  func createQRCodeFromGoogleAPI(from url: String) {
    guard let imageURL = URL(string: url) else { return }
    
    // just not to cause a deadlock in UI!
    DispatchQueue.global().async {
      guard let imageData = try? Data(contentsOf: imageURL) else { return }
      let image = UIImage(data: imageData)
      DispatchQueue.main.async {
        self.myQRCode.image = image
      }
    }
  }
  
  
  func generateQRCode(from string: String) -> UIImage? {
    let data = string.data(using: String.Encoding.ascii)
    
    if let filter = CIFilter(name: "CIQRCodeGenerator") {
      filter.setValue(data, forKey: "inputMessage")
      let transform = CGAffineTransform(scaleX: 3, y: 3)
      
      if let output = filter.outputImage?.transformed(by: transform) {
        return UIImage(ciImage: output)
      }
    }
    return nil
  }
  
  func downloadData() {
    let jsonURL = "http://localhost:8080/api/company"
    
    AF.request(jsonURL).responseJSON { (response) in
      if let err = response.error {
        print("Download Error: ", err)
        return
      }
      if let data = response.data {
        let decoder = JSONDecoder()
        let result = try? decoder.decode([Company].self, from: data)
        print("result 數量： \(result!.count)")
        var text = ""
        for i in 0...result!.count-1 {
          text = text + String(result![i].companyId) + " "
          text = text + result![i].companyName + "\n "
        }
        self.myTextView.text = text
      }
    }
  }
}

