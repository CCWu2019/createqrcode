//
//  Company.swift
//  createQRCode
//
//  Created by CCWu on 2019/12/26.
//  Copyright © 2019 吳稚賢. All rights reserved.
//

struct Company: Codable {
  var id: Int?
  var companyId: Int
  var companyName: String
}

